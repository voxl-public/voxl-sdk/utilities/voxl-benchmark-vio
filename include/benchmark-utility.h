#ifndef BENCHMARK_UTILITY_H
#define BENCHMARK_UTILITY_H

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <string>
#include <vector>

#include <fstream>
#include <sstream>

#include <iostream>
#include<experimental/filesystem>
#include <vft_interface.h>
#include <cJSON.h>
#include <algorithm>
#include <map>
#include <modalcv.h>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>
#include <sys/stat.h>
#include <dirent.h>
#include <string>
#include <iomanip>
#include "feature-analyzer.h"

namespace fs = std::experimental::filesystem;


static int64_t _apps_time_monotonic_ns() {
    struct timespec ts;
    if (clock_gettime(CLOCK_MONOTONIC, &ts)) {
        fprintf(stderr, "ERROR calling clock_gettime\n");
        return -1;
    }
    return (int64_t)ts.tv_sec * 1000000000 + (int64_t)ts.tv_nsec;
}

/**
 * @brief Reads file contents into string for later processing
 */
std::string read_file(const std::string& path) {
    std::ifstream file(path);
    if (!file.is_open()) {
        std::cerr << "Could not open file: " << path << std::endl;
        return "";
    }

    std::stringstream buffer;
    buffer << file.rdbuf();
    return buffer.str();
}


/**
 * @brief Concatenates two file paths
 */
std::string concatenate_paths(const std::string& path1, const std::string& path2) {
    if (path1.empty()) { return path2; }
    if (path2.empty()) { return path1; }

    std::string result = path1;
    if (result.back() == '/') {
        result.pop_back();
    }

    if (path2.front() == '/') {
        result += path2;
    } else {
        result += '/' + path2;
    }

    return result;
}

/**
 * NOTE: feature points should be sorted first by highest R > 0 (corners) 
 * and then large magnitude R < 0 (edges)
 */
float harris_corner_measure(const cv::Mat& img, int x, int y) {
    float k = 0.04;

    cv::Mat Ix, Iy;
    cv::Sobel(img, Ix, CV_32F, 1, 0, 3);
    cv::Sobel(img, Iy, CV_32F, 0, 1, 3);


    float sum_Ix2 = 0.f, sum_Iy2 = 0.f, sum_Ixy = 0.f;

    for (int i = -1; i <= 1; ++i) {
        for (int j = -1; j <= 1; ++j) {
            int xi = x + i;
            int yj = y + j;

            if (xi < 0 || xi >= img.cols || yj < 0 || yj >= img.rows) {
                continue;
            }

            float ix = Ix.at<float>(yj, xi);
            float iy = Iy.at<float>(yj, xi);

            // printf("x: %d, y: %d, x,y: %d, dx: %f, dy: %f\n", x, y, static_cast<int>(img.at<uchar>(yj, xi)), ix, iy);

            sum_Ix2 += ix * ix;
            sum_Iy2 += iy * iy;
            sum_Ixy += ix * iy;
        }
    }

    float detM = (sum_Ix2 * sum_Iy2) - (sum_Ixy * sum_Ixy);
    float traceM = sum_Ix2 + sum_Iy2;

    float R = detM - k * (traceM * traceM);
    return R;
}

/**
 * @brief Helper function to split a comma-separated string into a vector of strings
 */
std::vector<std::string> split_string(const std::string& str, char delimiter) {
    std::vector<std::string> tokens;
    std::stringstream ss(str);
    std::string item;

    while (std::getline(ss, item, delimiter)) {
        tokens.push_back(item);
    }

    return tokens;
}


/**
 * @brief Loads image paths from voxl log into string vector
 */
int _get_images_from_log(const std::string input_folder,
                                std::map<std::string, std::vector<std::string>>& img_paths,
                                int& w, int& h, int start_frame, std::vector<std::string> pipes) {
    
    std::string json_path = input_folder + "info.json";
    printf("json path: %s\n", json_path.c_str());
    std::string file_content = read_file(json_path);
    if (file_content.empty()) {
        return 1;
    }

    cJSON *json = cJSON_Parse(file_content.c_str());
    if (!json) {
        std::cerr << "Error parsing JSON: " << cJSON_GetErrorPtr() << std::endl;
        return 1;
    }

    cJSON *channels = cJSON_GetObjectItem(json, "channels");
    if (!cJSON_IsArray(channels)) {
        std::cerr << "'channels' is not an array" << std::endl;
        cJSON_Delete(json);
        return 1;
    }

    int cam_count = 0;
    cJSON *channel;
    cJSON_ArrayForEach(channel, channels) {
        cJSON *type_string = cJSON_GetObjectItem(channel, "type_string");
        if (cJSON_IsString(type_string) && std::string(type_string->valuestring) == "cam") {
            cam_count++;

            cJSON *pipe_path = cJSON_GetObjectItem(channel, "pipe_path");
            img_paths[pipe_path->valuestring];
        }
    }

    for (const auto& pair : img_paths) {

        std::string key = pair.first;
        std::string pipe_str = split_string(key, '/').back();

        auto it = std::find(pipes.begin(), pipes.end(), pipe_str);
        if (it == pipes.end()) {
            continue;
        }

        printf("cam: %s\n", key.c_str());
        std::string p = concatenate_paths(input_folder, key);

        const std::string extension = ".png";
        try {
            for (const auto& entry : fs::directory_iterator(p)) {
                // check if entry is regular file and matches extension
                if (fs::is_regular_file(entry) &&
                    entry.path().extension() == extension) {

                    std::string filename = entry.path().stem().string();
                    int frame_number = std::stoi(filename);

                    if (frame_number >= start_frame) {
                        img_paths[key].push_back(p + entry.path().filename().string());
                    }
                }
            }
            // sort the filenames
            std::sort(img_paths[key].begin(), img_paths[key].end());
        } catch (const std::exception& e) {
            std::cerr << "Error: " << e.what() << std::endl;
            return 1;
        }
        
        cv::Mat tmp_img =
            cv::imread(img_paths[key][0], cv::IMREAD_GRAYSCALE);
        w = tmp_img.cols;
        h = tmp_img.rows;
    }

    return 1;
}


/**
 * @brief reads file contents after comment at the top, useful for 
 *        .conf files which are json with a comment at the top
 */
std::string read_file_after_comment(const std::string& path) {
    std::ifstream file(path);
    if (!file.is_open()) {
        std::cerr << "Could not open file: " << path << std::endl;
        return "";
    }

    std::stringstream buffer;
    std::string line;
    bool in_comment = false;

    while (std::getline(file, line)) {
        // Check for the start of the comment
        if (!in_comment) {
            size_t start_pos = line.find("/**");
            if (start_pos != std::string::npos) {
                in_comment = true;
            }
        }

        // Check for the end of the comment
        if (in_comment) {
            size_t end_pos = line.find("*/");
            if (end_pos != std::string::npos) {
                in_comment = false;
                // Add the rest of the line after the comment end
                buffer << line.substr(end_pos + 2) << '\n';
                continue;
            }
        } else {
            buffer << line << '\n';
        }
    }
    return buffer.str();
}

/**
 * @brief Returns true if a directory exists
 */
bool directory_exists(const std::string& path) {
    struct stat info;
    if (stat(path.c_str(), &info) != 0) {
        return false; // Directory does not exist
    }
    return (info.st_mode & S_IFDIR) != 0; // Check if it's a directory
}

/**
 * @brief Creates directories and parent directories as necessary from the provided path
 */
bool create_directories(const std::string& path) {
    mode_t mode = 0755;  // Permissions for the new directories
    std::string current_path;
    size_t position = 0;
    while ((position = path.find_first_of("/\\", position)) != std::string::npos) {
        current_path = path.substr(0, position++);
        if (!current_path.empty() && !directory_exists(current_path)) {
            if (mkdir(current_path.c_str(), mode) != 0) {
                if (errno != EEXIST) {
                    std::cerr << "Error creating directory: " << strerror(errno) << std::endl;
                    return false;
                }
            }
        }
    }
    if (!directory_exists(path)) {
        if (mkdir(path.c_str(), mode) != 0) {
            std::cerr << "Error creating directory: " << strerror(errno) << std::endl;
            return false;
        }
    }
    return true;
}

// Function to list files in a directory
std::vector<std::string> get_files_in_directory(const std::string& directory) {
    std::vector<std::string> files;
    DIR* dirp = opendir(directory.c_str());
    if (dirp == NULL) {
        perror("opendir");
        return files;
    }
    struct dirent* dp;
    while ((dp = readdir(dirp)) != NULL) {
        if (dp->d_type == DT_REG) { // if regular file
            files.push_back(directory + "/" + dp->d_name);
        }
    }
    closedir(dirp);
    return files;
}


/** 
 * @brief Removes all files and directories in a directory
 */
bool remove_all_in_directory(const std::string& directory) {
    DIR* dir = opendir(directory.c_str());
    if (!dir) {
        std::cerr << "Error opening directory: " << directory << std::endl;
        return false;
    }

    struct dirent* entry;
    while ((entry = readdir(dir)) != nullptr) {
        std::string file_name = entry->d_name;
        if (file_name == "." || file_name == "..") {
            continue;  // Skip current and parent directories
        }

        std::string file_path = directory + "/" + file_name;

        struct stat path_stat;
        if (stat(file_path.c_str(), &path_stat) != 0) {
            std::cerr << "Error getting information for: " << file_path << std::endl;
            closedir(dir);
            return false;
        }

        if (S_ISDIR(path_stat.st_mode)) {
            // Recursively remove the contents of the directory
            if (!remove_all_in_directory(file_path)) {
                closedir(dir);
                return false;
            }
            // Remove the empty directory itself
            if (rmdir(file_path.c_str()) != 0) {
                std::cerr << "Failed to remove directory: " << file_path << std::endl;
                closedir(dir);
                return false;
            }
        } else {
            // Remove the file
            if (unlink(file_path.c_str()) != 0) {
                std::cerr << "Failed to delete file: " << file_path << std::endl;
                closedir(dir);
                return false;
            }
        }
    }

    closedir(dir);
    return true;
}


/**
 * @brief Returns camera id associated with specific camera pipe based on cofig file 
 */
int _find_camera_id(const std::string& file_path, const std::string& target_input_pipe) {
    // Read the JSON file
    std::string file_content = read_file_after_comment(file_path);
    if (file_content.empty()) {
        return -1;
    }

    // Parse the JSON file
    cJSON *json = cJSON_Parse(file_content.c_str());
    if (!json) {
        std::cerr << "Error parsing JSON: " << cJSON_GetErrorPtr() << std::endl;
        return -1;
    }

    // Get the "groups" array
    cJSON *groups = cJSON_GetObjectItem(json, "groups");
    if (!cJSON_IsArray(groups)) {
        std::cerr << "Invalid JSON format: 'groups' key not found or is not an array!" << std::endl;
        cJSON_Delete(json);
        return -1;
    }

    // get pipe name
    std::vector<std::string> result;
    std::stringstream ss(target_input_pipe);
    std::string item;

    while (std::getline(ss, item, '/')) {
        result.push_back(item);
    }
    std::string pipe_name = result.back();

    int camera_index = 0;
    cJSON *group = nullptr;
    cJSON_ArrayForEach(group, groups) {
        cJSON *group_cams = cJSON_GetObjectItem(group, "group_cams");
        if (cJSON_IsArray(group_cams)) {
            cJSON *camera = nullptr;
            cJSON_ArrayForEach(camera, group_cams) {
                cJSON *input_pipe = cJSON_GetObjectItem(camera, "input_pipe");
                if (cJSON_IsString(input_pipe) && input_pipe->valuestring == pipe_name) {
                    cJSON_Delete(json);
                    return camera_index;
                }
                ++camera_index;
            }
        }
    }

    cJSON_Delete(json);
    // Return -1 if the camera with the specific input_pipe name was not found
    return -1;
}

/**
 * @brief Draws feature overlay onto image based on provided features
 */
int draw_overlay( cv::Mat cur_frame,
    std::vector<vft_feature> feats_out, cv::Mat &overlay) {
    int baseline = 0;
    float font_size = 1.25;

    // ASSUMING DRAWING OVERLAY FOR 1 IMAGE AT A TIME
    int n_images = 1;


    std::vector<float> feat_measures;
    for(const auto& feat : feats_out) {
        feat_measures.push_back(harris_corner_measure(cur_frame, feat.x, feat.y));
    }

    cv::cvtColor(cur_frame, overlay, cv::COLOR_GRAY2RGB);

    for(int i = 0; i < (int)feats_out.size(); i++) {
        int age = feats_out[i].age;
        int radius, thickness;
        if(age > 10) {
            radius = 4;
            thickness = 3;
        } else {
            radius = 2;
            thickness = 2;
        }
        cv::Scalar color(std::max(0, 255-5*age), 255, 0);
        cv::Rect rect(feats_out[i].x-radius, feats_out[i].y-radius, radius*2, radius*2);
        cv::rectangle(overlay, rect, color, thickness, 1, 0);


        std::ostringstream ss;
        ss << std::fixed << std::setprecision(2) << feat_measures[i];
        std::string text = ss.str();

        // Define the position for the text (slightly offset from the feature point)
        //cv::Point textPos(feats_out[i].x + 10, feats_out[i].y - 10);
        //cv::putText(overlay, text, textPos, cv::FONT_HERSHEY_SIMPLEX, 0.5, cv::Scalar(0, 255, 0), 1, cv::LINE_AA);
        /*if (active_cam >= 0  && i < 20)c
        {
            char str[32];
            sprintf(str, "%d(%d)", (int)feats_out[i].id, (int)feats_out[i].age);
            cv::Size text_size = cv::getTextSize(str, cv::FONT_HERSHEY_COMPLEX,
                    font_size, font_size, &baseline);

            cv::putText(
                    overlay, //target image
                    str, //text
                    cv::Point((feats_out[i].x - radius - text_size.width),
                            feats_out[i].y - radius-text_size.height),
                    cv::FONT_HERSHEY_COMPLEX, font_size,
                    cv::Scalar(255, 255, 255), //font color
                    2.0, cv::LINE_AA);
        }*/
    }
}


typedef struct log_test_struct_t{
    std::string url;
    std::string path;
    std::string id;
    int start_frame;
    float finish_time;
    std::vector<std::string> pipe_includes;
    double final_xyz[3];
    bool en_ext_tracker;
    bool en_gpu;
} log_test_struct_t;


/**
 * @brief Returns a vector of test structs built from a log test json
 */
std::vector<log_test_struct_t> parse_log_test_json_file(const std::string& file_path) {

    std::vector<log_test_struct_t> tests;

    std::string file_content = read_file(file_path);

    cJSON* json = cJSON_Parse(file_content.c_str());
    if (!json) {
        std::cerr << "Error parsing JSON: " << cJSON_GetErrorPtr() << std::endl;
        return tests;
    }

    cJSON* tests_array = cJSON_GetObjectItem(json, "replay_defs");
    if (!cJSON_IsArray(tests_array)) {
        std::cerr << "Invalid JSON format: 'replay_defs' should be an array." << std::endl;
        cJSON_Delete(json);
        return tests;
    }

    cJSON* test_item;
    cJSON_ArrayForEach(test_item, tests_array) {
        log_test_struct_t test;

        cJSON* url = cJSON_GetObjectItem(test_item, "url");
        if (cJSON_IsString(url)) {
            test.url = url->valuestring;
        }

        cJSON* id = cJSON_GetObjectItem(test_item, "id");
        if (cJSON_IsString(id)) {
            test.id = id->valuestring;
        }

        cJSON* pipe_includes = cJSON_GetObjectItem(test_item, "pipe_includes");
        if (cJSON_IsString(pipe_includes)) {
            test.pipe_includes = split_string(pipe_includes->valuestring, ',');
        }

        cJSON* ext_tracker = cJSON_GetObjectItem(test_item, "en_ext_tracker");
        if (cJSON_IsBool(ext_tracker)) {
            test.en_ext_tracker = cJSON_IsTrue(ext_tracker);
        } else {
            test.en_ext_tracker = false;
        }

        cJSON* en_gpu = cJSON_GetObjectItem(test_item, "en_gpu");
        if (cJSON_IsBool(ext_tracker)) {
            test.en_gpu = cJSON_IsTrue(en_gpu);
        } else {
            test.en_gpu = false;
        }

        // cJSON *final_xyz = cJSON_GetObjectItem(test_item, "final_xyz");
        // if (!cJSON_IsArray(final_xyz)) {
        //     cJSON *x = cJSON_GetArrayItem(final_xyz, 0);
        //     cJSON *y = cJSON_GetArrayItem(final_xyz, 1);
        //     cJSON *z = cJSON_GetArrayItem(final_xyz, 2);
            
        //     test.final_xyz[0] = x->valuedouble;
        //     test.final_xyz[1] = y->valuedouble;
        //     test.final_xyz[2] = z->valuedouble;
        // } else {
        //     test.final_xyz[0] = 0.0;
        //     test.final_xyz[1] = 0.0;
        //     test.final_xyz[2] = 0.0;
        // }

        tests.push_back(test);
    }


    cJSON_Delete(json);

    return tests;
}

/**
 * @brief Function that reads and returns a JSON file
 */
cJSON* read_json_file(const fs::path& file_path) {
    std::ifstream file(file_path);
    if (!file.is_open()) {
        std::cerr << "Failed to open file: " << file_path << std::endl;
        return nullptr;
    }

    std::stringstream buffer;
    buffer << file.rdbuf();
    std::string content = buffer.str();

    cJSON* json = cJSON_Parse(content.c_str());
    if (!json) {
        std::cerr << "Error parsing JSON: " << cJSON_GetErrorPtr() << std::endl;
    }
    return json;
}

/**
 * @brief Checks if file at provided path is a directory
 */
bool is_directory(const std::string& path) {
    struct stat info;
    if (stat(path.c_str(), &info) != 0) {
        return false; // Error accessing the path
    }
    return (info.st_mode & S_IFDIR) != 0;
}


/**
 * @brief Function that combines JSON objects
 */
void combine_json_objects(cJSON* combined, cJSON* current) {
    if (!combined || !current) return;

    cJSON* current_item = nullptr;
    cJSON_ArrayForEach(current_item, current) {
        const char* key = current_item->string;

        cJSON* combined_item = cJSON_GetObjectItem(combined, key);
        if (combined_item) {
            // Merge the objects if the key already exists
            if (cJSON_IsObject(combined_item) && cJSON_IsObject(current_item)) {
                combine_json_objects(combined_item, current_item);
            }
        } else {
            // Otherwise, just add the new item to the combined JSON
            cJSON_AddItemToObject(combined, key, cJSON_Duplicate(current_item, 1));
        }
    }
}


/**
 * @brief lists directories within a given parent folder
 */
std::vector<std::string> list_directories(const std::string& parent_folder) {
    std::vector<std::string> directories;
    DIR* dir = opendir(parent_folder.c_str());
    if (dir) {
        struct dirent* entry;
        while ((entry = readdir(dir)) != nullptr) {
            std::string name = entry->d_name;
            if (name == "." || name == "..") {
                continue; // Skip current and parent directories
            }

            std::string full_path = parent_folder + name;
            if (is_directory(full_path)) {
                directories.push_back(full_path);
            }
        }
        closedir(dir);
    } else {
        std::cerr << "Error opening directory: " << parent_folder << std::endl;
    }
    return directories;
}

/**
 * @brief copies folder contents from source folder path to a destination folder path
 */
void copy_folder_with_extension(const fs::path& source, const fs::path& destination, 
                                const std::string& extension) {
    // Check if the source directory exists
    if (!fs::exists(source) || !fs::is_directory(source)) {
        std::cerr << "Source directory " << source << " does not exist or is not a directory.\n";
        return;
    }

    // Create the destination directory if it doesn't exist
    if (!fs::exists(destination)) {
        fs::create_directories(destination);
    }

    // Iterate over each item in the source directory
    for (const auto& entry : fs::recursive_directory_iterator(source)) {
        const auto& path = entry.path();
        
        // Calculate relative path manually by removing the source prefix from path
        auto relativePath = path.string().substr(source.string().length());
        auto destinationPath = destination / relativePath;

        try {
            // Check if the file has the specified extension
            if (fs::is_regular_file(path) && path.extension() == ("." + extension)) {
                fs::copy(path, destinationPath, fs::copy_options::overwrite_existing);
            }
        } catch (fs::filesystem_error& e) {
            std::cerr << "Error copying file " << path << ": " << e.what() << '\n';
        }
    }
}

/**
 * @brief writes a json object to provided path
 */
void write_summary_json(const std::string& file_path, cJSON* combined_json) {
    if (!combined_json) return;

    char* json_string = cJSON_Print(combined_json);
    if (!json_string) {
        std::cerr << "Failed to print JSON" << std::endl;
        return;
    }

    std::ofstream file(file_path);
    if (!file.is_open()) {
        std::cerr << "Failed to create summary file: " << file_path << std::endl;
        free(json_string);
        return;
    }

    file << json_string;
    free(json_string);

    std::cout << "\nOuput Summary: " << file_path << "\n";
}

void update_progress_bar(int progress, int total) {
    int bar_width = 50; // Width of the progress bar

    // Calculate the percentage of completion
    float percentage = static_cast<float>(progress) / total;
    int pos = static_cast<int>(bar_width * percentage);

    // Create the progress bar string
    std::cout << "[";
    for (int i = 0; i < bar_width; ++i) {
        if (i < pos) {
            std::cout << "=";  // Filled part
        } else if (i == pos) {
            std::cout << ">";  // Current position
        } else {
            std::cout << " ";  // Empty part
        }
    }
    std::cout << "] " << int(percentage * 100.0) << " %\r"; // Print the percentage
    std::cout.flush();  // Ensure the progress bar is printed immediately
}

void add_element_to_json(const std::string& file_path, const std::string& name, int value) {
    cJSON* json = read_json_file(file_path);
    if (json == nullptr) {
        std::cerr << "Error parsing JSON." << std::endl;
        return;
    }

    cJSON* new_item = cJSON_CreateNumber(value);
    if (new_item == nullptr) {
        std::cerr << "Error creating new JSON item." << std::endl;
        cJSON_Delete(json);
        return;
    }

    // Add the new item to the JSON object
    cJSON_AddItemToObject(json, name.c_str(), new_item);

    // Convert the updated cJSON object back to a string
    char* updated_json_string = cJSON_Print(json);
    if (updated_json_string == nullptr) {
        std::cerr << "Error converting updated JSON to string." << std::endl;
        cJSON_Delete(json);
        return;
    }

    std::ofstream file(file_path);
    if (!file.is_open()) {
        std::cerr << "Error opening file for writing: " << file_path << std::endl;
        return;
    }
    file << updated_json_string;

    // Clean up
    cJSON_Delete(json);
    free(updated_json_string);

    return;
}


#endif