#ifndef FEATURE_ANALYZER_H
#define FEATURE_ANALYZER_H

#include <modalcv.h>
#include <unistd.h>
#include <map>
#include <vft_interface.h>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/imgproc.hpp>

const int   n_max_cameras   = 4;

struct vft_metrics_t {

    // total summed age of all features over all timesteps
    int feature_age_sum;

    // total number of features across all timesteps
    int n_features;

    // init above vals to 0
    vft_metrics_t() {
        feature_age_sum = 0;
        n_features = 0;
    }
};

mcv_cvp_feature_tracker_config_t init_default_feature_tracker_config(int w, int h);

void get_metrics_from_vft_data(std::map<int, vft_metrics_t>& vft_map, 
                               vft_feature_packet* feature_packet,
                               vft_feature* features, int n_rec_packets);

int _write_metrics_to_json(std::string output_path, std::map<int, vft_metrics_t> vft_map);

#endif