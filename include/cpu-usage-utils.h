#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>
#include <thread>
#include <chrono>

// Reads total jiffies from /proc/stat (line "cpu  ...")
bool read_system_stat(unsigned long long &total_jiffies)
{
    std::ifstream file("/proc/stat");
    if (!file.is_open())
        return false;

    std::string line;
    if (!std::getline(file, line))
        return false;

    // line should start with "cpu"
    std::istringstream iss(line);
    std::string label;
    iss >> label;

    if (label.compare("cpu") != 0)
        return false;

    unsigned long long value, sum = 0ULL;
    while (iss >> value) {
        sum += value;
    }
    total_jiffies = sum;
    return true;
}

// Reads utime & stime for a PID from /proc/[pid]/stat
bool read_proc_stat(int pid, unsigned long long &utime, unsigned long long &stime)
{
    std::string path = "/proc/" + std::to_string(pid) + "/stat";
    std::ifstream ifs(path);
    if (!ifs.is_open())
        return false;

    std::string line;
    if (!std::getline(ifs, line))
        return false;

    std::istringstream iss(line);
    std::vector<std::string> tokens;
    std::string token;
    while (iss >> token) {
        tokens.push_back(token);
    }

    // utime is field #14, stime is field #15 (1-based), so indices 13,14 in 0-based
    if (tokens.size() < 15) {
        return false;
    }

    try {
        utime = std::stoull(tokens[13]);
        stime = std::stoull(tokens[14]);
    } catch (...) {
        return false;
    }
    return true;
}


struct cpu_usage_data 
{
    bool   keep_running = true;
    double usage_sum    = 0.0;
    int    samples      = 0;
    std::vector<double> usage_samples;
};

double compute_median(std::vector<double> &samples)
{
    if (samples.empty()) {
        return 0.0;
    }

    // Sort the samples
    std::sort(samples.begin(), samples.end());
    
    size_t n = samples.size();
    if (n % 2 == 1) {
        // Odd count
        return samples[n / 2];
    } else {
        // Even count
        double mid1 = samples[(n / 2) - 1];
        double mid2 = samples[n / 2];
        return 0.5 * (mid1 + mid2);
    }
}


void track_cpu_usage(pid_t pid, cpu_usage_data* data)
{

    unsigned long long old_utime=0, old_stime=0, old_total=0;

    // Try initial read
    if (!read_proc_stat(pid, old_utime, old_stime) || !read_system_stat(old_total)) 
    {
        printf("Initial CPU read failed for PID %d\n", pid);
        data->keep_running = false;
        return;
    }

    while (data->keep_running) 
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        // Attempt to read new stats
        unsigned long long cur_utime=0, cur_stime=0, cur_total=0;
        if (!read_proc_stat(pid, cur_utime, cur_stime)) 
        {
            // Probably the process ended
            printf("PID %d ended (CPU monitor stopping)\n", pid);
            break;
        }
        if (!read_system_stat(cur_total)) 
        {
            printf("Failed to read /proc/stat\n");
            break;
        }

        // Compute deltas
        unsigned long long delta_proc = (cur_utime - old_utime) + (cur_stime - old_stime);
        unsigned long long delta_total = (cur_total - old_total);

        double usage_percent = 0.0;
        if (delta_total > 0)
        {
            usage_percent = 100.0 * double(delta_proc) / double(delta_total);
        }
        data->usage_samples.push_back(usage_percent);

        data->usage_sum += delta_proc;
        data->samples += delta_total;

        // Update old
        old_utime = cur_utime;
        old_stime = cur_stime;
        old_total = cur_total;
    }

    // When we exit, either the process ended or keepRunning was set to false
    printf("CPU monitor thread for PID %d stopped\n", pid);
}
