# voxl-benchmark-vio

Utility for benchmarking VIO performance on VOXL


## Build Instructions

### Docker Image

This required the voxl-cross docker image >=V1.7

Follow the instructions here to build and install the voxl-cross docker image:

https://gitlab.com/voxl-public/voxl-docker

Launch the voxl-cross docker.

```bash
~/git/libmodal-cv$ voxl-docker -i voxl-cross
voxl-cross:~$ 
```

### Build for APQ 8096 

1) Install dependencies inside the docker. Specify the dependencies should be pulled from either the development (dev) or stable modalai package repos. If building the master branch you should specify `stable`, otherwise `dev`.

```bash
voxl-cross:~$ ./install_build_deps.sh apq8096 stable
```


2) This will build for APQ8096 with GCC 4.9 and the CDSP components disabled. Optionally clean before build

```bash
voxl-cross:~$ ./clean.sh
voxl-cross:~$ ./build.sh apq8096
```

3) Make an ipk package for APQ8096 while still inside the docker.

```bash
voxl-cross:~$ ./make_package.sh ipk
```

### Build for QRB5165

1) Install dependencies inside the docker. If building the master branch you should specify `staging`, otherwise `dev`.

```bash
voxl-cross:~$ ./install_build_deps.sh qrb5165 staging
```


2) This will build for QRB51656 with GCC 7 and the CDSP components enabled. Optionally clean before build

```bash
voxl-cross:~$ ./clean.sh
voxl-cross:~$ ./build.sh qrb5165
```

3) Make a deb package for QRB5165 while still inside the docker. Optionally add the --timestamp argument to append the current data and time to the package version number in the debian package. This is done automatically by the CI package builder for development and nightly builds, however you can use it yourself if you like.

```bash
voxl-cross:~$ ./make_package.sh deb
```


## Deploy to VOXL

You can now push the ipk or deb package to VOXL and install with dpkg/opkg however you like. To do this over ADB, you may use the included helper script: deploy_to_voxl.sh. Do this outside of docker as your docker image probably doesn't have usb permissions for ADB.

The deploy_to_voxl.sh script will query VOXL over adb to see if it has dpkg installed. If it does then then .deb package will be pushed an installed. Otherwise the .ipk package will be installed with opkg.

```bash
(outside of docker)
libmodal-cv$ ./deploy_to_voxl.sh
```

This deploy script can also push over a network given sshpass is installed and the VOXL uses the default root password.


```bash
(outside of docker)
libmodal-cv$ ./deploy_to_voxl.sh ssh 192.168.1.123
```
