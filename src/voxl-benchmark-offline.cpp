#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <string>
#include <vector>

#include <fstream>
#include <sstream>

#include <iostream>
#include <iomanip>
#include<experimental/filesystem>
#include <cJSON.h>
#include <algorithm>
#include <sys/stat.h>
#include <linux/perf_event.h>
#include <asm/unistd.h>
#include <sys/ioctl.h>

#include "feature-analyzer.h"
#include "benchmark-utility.h"

namespace fs = std::experimental::filesystem;

const std::string BASE_PATH = "/data/benchmark-results/";
const std::string BENCHMARK_LOGS_FOLDER_PATH = "/data/benchmark-logs/";

std::string OUTPUT_FOLDER_NAME = "res/";


const int max_path_size   = 80;
char input_path[max_path_size];
char output_folder_name[max_path_size];
char replay_pipe_path[max_path_size];


void _print_usage() {
    printf(
"Offline benchmark for SITL feature tracker testing\n\
\n\
-h, --help                  print this help message\n\
-p, --path {path}           complete path to json, e.g. /data/myfile.json\n\
-o, --output {name}         name of output folder created in /data/benchmark-results/\n\
-f, --features_ov           flag to include frames with features drawn in output\n\
-r, --replay {path}         complete path to folder with output images for portal replay\n\
                            generated from -f flag\n\
\n\
NOTE: can only use -p or -r, not both at the same time\n\
\n");
return;
}

#define REPLAY_FPS 30
#define N_FEATURES_TO_PUBLISH 30

bool features_ov_flag = false;
bool output_folder_flag = false;
bool replay_flag = false;
float start_time = 0.f;

static int _parse_opts(int argc, char *argv[]) {
    static struct option long_options[] = {
        {"path", required_argument, 0, 'p'},
        {"output", required_argument, 0, 'o'},
        {"replay", required_argument, 0, 'r'},
        {"help", no_argument, 0, 'h'},
        {"features_ov", no_argument, 0, 'f'},
        {0, 0, 0, 0}
    };

    int path_flag = 0;  // flag to check if path option is provided

    while (1) {
        int option_index = 0;
        int c = getopt_long(argc, argv, "hp:fo:r:", long_options, &option_index);

        if (c == -1) break;  // Detect the end of the options.

        switch (c) {
            case 0:
                // for long args without short equivalent that just set a flag
                // nothing left to do so just break.
                if (long_options[option_index].flag != 0) break;
                break;
            case 'p':
                path_flag = 1;
                printf("Input JSON: %s\n", optarg);
                strncpy(input_path, optarg, max_path_size);
                break;
            case 'o':
                output_folder_flag = true;
                printf("Output Folder: %s\n", optarg);
                strncpy(output_folder_name, optarg, max_path_size);
                break;
            case 'h':
                _print_usage();
                exit(0);
            case 'f':
                features_ov_flag = true;
                break;
            case 'r':
                replay_flag = true;
                strncpy(replay_pipe_path, optarg, max_path_size);
                break;
            case '?':
                // If option is missing argument or unknown option
                _print_usage();
                exit(0);
            default:
                _print_usage();
                exit(0);
        }
    }

    // check if path option is provided
    if (!path_flag && !replay_flag) {
        printf("Error: Missing or incorrect arguments.\n");
        _print_usage();
        return -1;
    }
    return 0;
}


void do_preprocessing(const cv::Mat& img)
{
	// smooth out input image
	GaussianBlur(img, img, cv::Size(5, 5), 0);

	// Resize the image
	int scale = 32;
	cv::Mat resizedImg;
	cv::resize(img, resizedImg, cv::Size(img.cols/scale, img.rows/scale));

	// blur the downsized image
	cv::Mat blurredImg;
	cv::GaussianBlur(resizedImg, blurredImg, cv::Size(9, 9), 0);

	// Upscale the blurred image back to the original size
	cv::Mat upscaledBlurredImg;
	cv::resize(blurredImg, upscaledBlurredImg, img.size());

	// Convert upscaled blurred image to 16-bit
	cv::Mat upscaledBlurred16bit;
	upscaledBlurredImg.convertTo(upscaledBlurred16bit, CV_16U, 2);  // Convert blurred image to 16-bit

	// Convert the original image to 16-bit
	cv::Mat img16bit;
	img.convertTo(img16bit, CV_16U, 256);  // Convert original image to 16-bit

	// Perform element-wise division (note: OpenCV does not support direct division of different types)
	cv::Mat dividedImg;
	cv::divide(img16bit, upscaledBlurred16bit, dividedImg, 1.0, CV_16U);  // Using CV_16U to keep results in 16-bit space

	// Normalize to 8-bit range
	dividedImg.convertTo(img, CV_8U, 1.0);

	return;
}

double computeLaplacianVariance(const cv::Mat& image) {
    cv::Mat gray, laplacian;
    gray = image.clone();
    cv::Laplacian(gray, laplacian, CV_64F);         // Compute Laplacian
    cv::Scalar mean, stddev;
    cv::meanStdDev(laplacian, mean, stddev);        // Compute mean and standard deviation
    return stddev[0] * stddev[0];                   // Return the variance of the Laplacian
}

// Function to open a performance event
long perf_event_open(struct perf_event_attr *hw_event, pid_t pid,
                     int cpu, int group_fd, unsigned long flags) {
    return syscall(__NR_perf_event_open, hw_event, pid, cpu, group_fd, flags);
}


// maps cam_id (based on voxl-feature_tracker.conf) to relevant vft metrics
std::map<int, vft_metrics_t> vft_map;
std::map<int, long long> cpu_cycles_map;
std::map<int, long long> runtime_map;

// maps cam_id to the number of received frames
std::map<int, int> rec_frames;

// running count of how many vft packets have been received
int n_rec_packets = 0;

// structs for feature analysis
vft_feature_packet *feature_packet;
vft_feature *features;
std::vector<vft_feature> tracked_feats;

int main(__attribute__((unused)) int argc,
         __attribute__((unused)) char *argv[]) {

    if (_parse_opts(argc, argv) != 0) {
        return -1;
    }

    // REPLAY MODE CONDITION
    if (replay_flag) {

        // get image dimensions from path
        std::string img_input_path = std::string(replay_pipe_path);
        if(!directory_exists(img_input_path)) {
            printf("ERROR :: Directory not found: %s\n", img_input_path.c_str());
            return 0;
        }

        std::vector<std::string> img_paths;
        int width, height;
        
        const std::string extension = ".png";
        try {
            for (const auto& entry : fs::directory_iterator(img_input_path)) {
                // check if entry is regular file and matches extension
                if (fs::is_regular_file(entry) &&
                    entry.path().extension() == extension) {
                    img_paths.push_back(img_input_path + entry.path().filename().string());
                }
            }

            // sort the filenames
            std::sort(img_paths.begin(), img_paths.end());
        } catch (const std::exception& e) {
            std::cerr << "Error: " << e.what() << std::endl;
            return 1;
        }
        
        cv::Mat tmp_img =
            cv::imread(img_paths[0], cv::IMREAD_COLOR);
        cv::cvtColor(tmp_img, tmp_img, cv::COLOR_BGR2RGB);
        width = tmp_img.cols;
        height = tmp_img.rows;

        printf("num_imgs: %d, w: %d, h: %d\n", img_paths.size(), width, height);

         // logic for feature overlay pipe
        pipe_info_t overlay_info;
        overlay_info.size_bytes = 1280 * 800 * 15;
        overlay_info.server_pid = 0;

        strncpy(overlay_info.name, "benchmark_overlay", MODAL_PIPE_MAX_NAME_LEN);
        overlay_info.name[MODAL_PIPE_MAX_NAME_LEN - 1] = '\0';

        strncpy(overlay_info.location, "/run/mpa/benchmark_overlay", MODAL_PIPE_MAX_DIR_LEN);
        overlay_info.location[MODAL_PIPE_MAX_DIR_LEN - 1] = '\0';

        strncpy(overlay_info.type, "camera_image_metadata_t", MODAL_PIPE_MAX_TYPE_LEN);
        overlay_info.type[MODAL_PIPE_MAX_TYPE_LEN - 1] = '\0';

        int flags = SERVER_FLAG_EN_CONTROL_PIPE;

        if (pipe_server_create(0, overlay_info, flags)) {
            fprintf(stderr, "ERROR creating feature pipe\n");
        }

        int frame_id = 0;

        for (int i = 0; i < img_paths.size(); i++) {
            cv::Mat cur_img;
            cur_img = cv::imread(img_paths[i], cv::IMREAD_COLOR);

            if (cur_img.empty()) {
                printf("error loading img: %s\n", img_paths[i].c_str());
                return -1;
            }

            camera_image_metadata_t meta;
            meta.magic_number = CAMERA_MAGIC_NUMBER;
            meta.timestamp_ns = _apps_time_monotonic_ns();
            meta.frame_id = frame_id++;
            meta.width = width;
            meta.height = height;
            meta.size_bytes = width * height * 3; // need x3 for all 3 color channels
            meta.format = IMAGE_FORMAT_RGB;
            meta.framerate = 10;
            meta.gain = 100;

            if (sizeof(cur_img.data) > 0) {
                int rc = pipe_server_write_camera_frame(0, meta, (char*)cur_img.data);
                if (rc) {
                    printf("pipe srver write failed\n");
                }
            }

            // usleep(1000000 / REPLAY_FPS);
            usleep(100000);
        }
        
        pipe_server_close_all();

        return 0;
    }


    // create all the output folders
    if (!directory_exists(BASE_PATH)) {
        if (!create_directories(BASE_PATH)) {
            return -1; // Failed to create directory
        }
    }
    if (!directory_exists(BENCHMARK_LOGS_FOLDER_PATH)) {
        if (!create_directories(BENCHMARK_LOGS_FOLDER_PATH)) {
            return -1; // Failed to create directory
        }
    }
    if (output_folder_flag) {
        OUTPUT_FOLDER_NAME = concatenate_paths(output_folder_name, "/");
    }
    if (!directory_exists(concatenate_paths(BASE_PATH, OUTPUT_FOLDER_NAME))) {
        if (!create_directories(concatenate_paths(BASE_PATH, OUTPUT_FOLDER_NAME))) {
            return -1; // Failed to create directory
        }
    }
    if (directory_exists(concatenate_paths(BASE_PATH, OUTPUT_FOLDER_NAME))) {
        if (!remove_all_in_directory(concatenate_paths(BASE_PATH, OUTPUT_FOLDER_NAME))) {
            std::cerr << "Warning, failed to delete old files from " << concatenate_paths(BASE_PATH, OUTPUT_FOLDER_NAME) << std::endl;
            return -1;
        }
    }

    // parse json file to get info of logs being tested
    std::vector<log_test_struct_t> tests = parse_log_test_json_file(std::string(input_path)); 

    int test_count = 1;

    // Download and unpack the URL to local
    for(const auto& test : tests) {
        std::string log_name = split_string(test.url, '/').back();
        log_name = log_name.substr(0, log_name.size() - 7);

        printf("\nBENCHMARK :: %s { %d / %d }\n", log_name.c_str(), test_count, tests.size());
        test_count++;
        
        std::string log_path = concatenate_paths(BENCHMARK_LOGS_FOLDER_PATH, log_name);

        if (!directory_exists(log_path)) {
            std::string command = "wget -P " + BENCHMARK_LOGS_FOLDER_PATH + " " + test.url;
            int result = std::system(command.c_str());

            // Check if the command was successful
            if (result == 0) {
                printf("Successfully Downloaded: %s\n", test.url.c_str());
            } else {
                printf("Failed to Download: %s\n", test.url.c_str());
                return 0;
            }

            if (!create_directories(log_path)) {
                return -1; // Failed to create directory
            }

            std::string tar_path = log_path + ".tar.gz";
            command = "tar -xzf " + tar_path + " -C " + log_path;

            result = std::system(command.c_str());

            // Check if the command was successful
            if (result == 0) {
                std::cout << "Extracted " << tar_path << " to " << log_path << std::endl;
            } else {
                std::cerr << "Failed to extract " << tar_path << std::endl;
                return 0;
            }

            command = "rm " + tar_path;
            result = std::system(command.c_str());

            printf("Downloaded and unpacked: %s\n", test.url.c_str());
        } 
        else {
            printf("Log already exists: %s\n", log_path.c_str());
        }


        log_path = concatenate_paths(log_path, test.path) + "/";

        // get image dimensions from path
        std::map<std::string, std::vector<std::string>> img_paths;
        int width, height;
        _get_images_from_log(log_path, img_paths, width, height, test.start_frame, test.pipe_includes);

        // create feature tracker handler for the current log
        mcv_cvp_feature_tracker_config_t feature_tracker_cfg
            = init_default_feature_tracker_config(width, height);

        mcv_cvp_feature_tracker_handle feature_tracker_handle;
        feature_tracker_handle = mcv_cvp_feature_tracker_init(feature_tracker_cfg);
        if (NULL == feature_tracker_handle) {
            fprintf(stderr, "ERROR getting feature tracker handle\n");
            return -1;
        }

        create_vft_memory(&feature_packet, &features);

        int num_images_to_process = 0;
        int images_processed = 0;

        // put the cams in order
        std::vector<std::pair<std::string, int>> cam_order;
        for (const auto& pair : img_paths) {
            num_images_to_process += img_paths[pair.first].size() - 1; // first frame is skipped
            cam_order.push_back(std::make_pair(pair.first, _find_camera_id(log_path + "etc/modalai/voxl-feature-tracker.conf", pair.first)));
        }

        std::sort(cam_order.begin(), cam_order.end(), [](const std::pair<std::string, int>& a, const std::pair<std::string, int>& b) {
            return a.second < b.second; // Change to > for descending order
        });


        std::string result_path = concatenate_paths(BASE_PATH, OUTPUT_FOLDER_NAME) + test.id + "/";
        std::map<int, int> imgs_per_cam;

        // loop over each camera's output pipe
        int cam_index = 0;
        for (const auto& pair : cam_order) {
            std::string key = pair.first;

            feature_packet->cams_with_data = 0;
            int cam_id = pair.second;

            imgs_per_cam[cam_id] = img_paths[key].size();

            int img_count = 0;

            if (features_ov_flag) {
                if (!directory_exists(concatenate_paths(result_path, key))) {
                    if (!create_directories(concatenate_paths(result_path, key))) {
                        return -1; // Failed to create directory
                    }
                }

                std::string path_to_clear = concatenate_paths(result_path, key);
                if (directory_exists(path_to_clear)) {
                    if (!remove_all_in_directory(path_to_clear)) {
                        std::cerr << "Warning, failed to delete old files from " << path_to_clear << std::endl;
                        return -1;
                    }
                }
            }

            // Set up performance event attributes to count cpu cycles
            struct perf_event_attr pe;
            memset(&pe, 0, sizeof(struct perf_event_attr));
            pe.type = PERF_TYPE_HARDWARE;
            pe.size = sizeof(struct perf_event_attr);
            pe.config = PERF_COUNT_HW_CPU_CYCLES;   // Counting CPU cycles
            pe.disabled = 1;                        // Start disabled
            pe.exclude_kernel = 1;                  // Exclude kernel mode
            pe.exclude_hv = 1;                      // Exclude hypervisor mode

            // Open the performance event
            int fd = perf_event_open(&pe, 0, -1, -1, 0);
            if (fd == -1) {
                std::cerr << "Error opening perf event: " << strerror(errno) << std::endl;
                return 1;
            }

            long long total_cpu_cycles = 0;


            std::map<int, std::pair<int, int>> prev_selected_ids;
            std::vector<std::vector<int>> feature_distributions;

            // loop over all camera frames and run the feature tracker process
            bool first_frame = true;
            for (const auto& path : img_paths[key]) {
                update_progress_bar(images_processed, num_images_to_process);

                if (first_frame) {
                    first_frame = false;
                    continue;
                }

                cv::Mat cur_img;
                cur_img = imread(path, cv::IMREAD_GRAYSCALE);

                if(cur_img.type() != CV_8UC1) {
                    fprintf(stderr, "ERROR loading image: %s\n", path.c_str());
                    return -1;
                }

                // variables for feature tracker outputs
                int16_t num_outputs;
                mcv_cvp_feature_tracker_output_t output[MAX_N_FEATURES];

                ioctl(fd, PERF_EVENT_IOC_RESET, 0);   // Reset the counter before each function call
                ioctl(fd, PERF_EVENT_IOC_ENABLE, 0);  // Enable the counter

                // run process
                int64_t start = _apps_time_monotonic_ns();
                mcv_cvp_feature_tracker_process(0, feature_tracker_handle, cur_img.data,
                                                nullptr, 30, &prev_selected_ids, &feature_distributions, &num_outputs, output);
                int64_t end = _apps_time_monotonic_ns();
                runtime_map[cam_id] += (end - start);

                ioctl(fd, PERF_EVENT_IOC_DISABLE, 0); // Disable the counter

                // Read the performance counter value for this iteration
                long long cycle_count = 0;
                read(fd, &cycle_count, sizeof(long long));

                int N = MIN(num_outputs, N_FEATURES_TO_PUBLISH);

                feature_packet->cams_with_data |= (1U << cam_index);
                feature_packet->num_feats[cam_index] = N;
                cpu_cycles_map[cam_id] += cycle_count;

                // iterate through the output features, adding them to tracked_feats
                std::vector<vft_feature> tmp_tracked_feats;

                // convert mcv_cvp_feature to vft_feature
                for (int i = 0; i < N; i++) {
                    vft_feature feat;
                    feat.id = output[i].id;
                    feat.x = output[i].x;
                    feat.y = output[i].y;
                    feat.x_prev = output[i].x_prev;
                    feat.y_prev = output[i].y_prev;
                    feat.age = output[i].age;
                    feat.pyr_lvl_mask = output[i].pyr_lvl_mask;
                    feat.score = output[i].score;
                    feat.cam_id = cam_id;

                    tmp_tracked_feats.push_back(feat);
                }

                for(int i = 0; i < tmp_tracked_feats.size(); i++) {
                    features[i] = tmp_tracked_feats[i];
                }


                if (features_ov_flag) {
                    // output feature overlay output for later viewing/playback
                    cv::Mat overlay;
                    draw_overlay(cur_img, tmp_tracked_feats, overlay);
                    
                    std::stringstream ss;
                    ss << std::setw(5) << std::setfill('0') << rec_frames[cam_id];
                    
                    std::string output_path = concatenate_paths(result_path, key) + ss.str() + ".png";
                    cv::imwrite(output_path, overlay);
                }

                rec_frames[cam_id]++;
                images_processed++;
                get_metrics_from_vft_data(vft_map, feature_packet, features, rec_frames[cam_id]);
            }

            cam_index++;
            close(fd);
        }

        // write vft metrics to json
        std::string out_str = result_path + std::string("results.json");
        _write_metrics_to_json(out_str.c_str(), vft_map);
        vft_map.clear();

        cJSON *parent = json_read_file(out_str.c_str());
        cJSON *cpu_clocks  = cJSON_CreateObject();
        cJSON *runtime = cJSON_CreateObject();
        cJSON *avg_runtime = cJSON_CreateObject();

        for (const auto pair : cpu_cycles_map) {
            int cam_id = pair.first;
            long long cycle_count = pair.second;
            std::string str = "cam_" + std::to_string(cam_id);
            cJSON_AddNumberToObject(cpu_clocks, str.c_str(), cycle_count);
        }
        for (const auto pair : runtime_map) {
            int cam_id = pair.first;
            long long time = pair.second;
            double time_ms = time / 1000000.0;
            double avg_time_ms = time_ms / imgs_per_cam[cam_id];
            std::string str = "cam_" + std::to_string(cam_id);
            cJSON_AddNumberToObject(runtime, str.c_str(), time_ms); 
            cJSON_AddNumberToObject(avg_runtime, str.c_str(), avg_time_ms); 
        }

        cJSON_AddItemToObject(parent, "total_runtime_ms", runtime);
        cJSON_AddItemToObject(parent, "avg_runtime_ms", avg_runtime);
        // cJSON_AddItemToObject(parent, "total_cpu_cycles", cpu_clocks);

        json_write_to_file_with_header(out_str.c_str(), parent, "");

        mcv_cvp_feature_tracker_deinit(feature_tracker_handle);
        destroy_vft_memory(&feature_packet, &features);    
        cpu_cycles_map.clear();
        runtime_map.clear();
    }


    // combine the resulting output jsons into a summary
    cJSON* combined_json = cJSON_CreateObject();
    if (!combined_json) {
        std::cerr << "Failed to create combined JSON object" << std::endl;
        return 0;
    }

    // List all directories in the parent folder
    std::string path = concatenate_paths(BASE_PATH, OUTPUT_FOLDER_NAME);
    std::vector<std::string> directories = list_directories(path);

    // Iterate over each child directory and process results.json
    for (const auto& dir : directories) {
        std::string results_file = dir + "/results.json";
        if (std::ifstream(results_file)) { // Check if file exists
            cJSON* current_json = read_json_file(results_file);
            if (current_json) {
                std::string name = split_string(dir, '/').back();
                // Combine the current JSON with the combined JSON
                cJSON_AddItemToObject(combined_json, name.c_str(), current_json);
            }
        }
    }

    // Write the combined JSON to the results_summary.json file
    path = concatenate_paths(path, "results_summary.json");
    write_summary_json(path, combined_json);

    // Clean up
    cJSON_Delete(combined_json);

    return 0;
}