#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <thread>
#include <chrono>

// Reads total jiffies from /proc/stat (line "cpu  ...")
bool read_system_jiffies(unsigned long long &total_jiffies)
{
    std::ifstream file("/proc/stat");
    if (!file.is_open())
        return false;

    std::string line;
    if (!std::getline(file, line))
        return false;

    // line should start with "cpu"
    std::istringstream iss(line);
    std::string label;
    iss >> label;

    if (label.compare("cpu") != 0)
        return false;

    unsigned long long value, sum = 0ULL;
    while (iss >> value) {
        sum += value;
    }
    total_jiffies = sum;
    return true;
}

// Reads utime & stime for a PID from /proc/[pid]/stat
bool read_proc_times(int pid, unsigned long long &utime, unsigned long long &stime)
{
    std::string path = "/proc/" + std::to_string(pid) + "/stat";
    std::ifstream ifs(path);
    if (!ifs.is_open())
        return false;

    std::string line;
    if (!std::getline(ifs, line))
        return false;

    std::istringstream iss(line);
    std::vector<std::string> tokens;
    std::string token;
    while (iss >> token) {
        tokens.push_back(token);
    }

    // utime is field #14, stime is field #15 (1-based), so indices 13,14 in 0-based
    if (tokens.size() < 15) {
        return false;
    }

    try {
        utime = std::stoull(tokens[13]);
        stime = std::stoull(tokens[14]);
    } catch (...) {
        return false;
    }
    return true;
}

int main(int argc, char *argv[])
{
    if (argc < 2) {
        printf("Usage: %s <pid>\n", argv[0]);
        return 1;
    }

    int pid = std::stoi(argv[1]);

    // Initial read
    unsigned long long prev_utime = 0ULL, prev_stime = 0ULL;
    unsigned long long prev_total_jiffies = 0ULL;

    if (!read_proc_times(pid, prev_utime, prev_stime)) 
    {
        printf("Error: cannot read /proc/%d/stat.\n", pid);
        return 1;
    }
    if (!read_system_jiffies(prev_total_jiffies)) 
    {
        printf("Error: cannot read /proc/stat.\n");
        return 1;
    }

    double total_proc = 0, total_jiffies = 0;

    while (true)
    {
        std::this_thread::sleep_for(std::chrono::milliseconds(100));

        unsigned long long cur_utime = 0, cur_stime = 0;
        if (!read_proc_times(pid, cur_utime, cur_stime)) {
            printf("Process %d ended or permission denied.\n", pid);
            break;
        }

        unsigned long long cur_total_jiffies = 0ULL;
        if (!read_system_jiffies(cur_total_jiffies)) {
            printf("Cannot read /proc/stat.\n");
            break;
        }

        unsigned long long delta_proc = (cur_utime - prev_utime) + (cur_stime - prev_stime);
        unsigned long long delta_total = (cur_total_jiffies - prev_total_jiffies);

        double usage_percent = 0.0;
        if (delta_total > 0) {
            usage_percent = 100.0 * (double)delta_proc / (double)delta_total;
        }

        total_proc += delta_proc;
        total_jiffies += delta_total;

        prev_utime = cur_utime;
        prev_stime = cur_stime;
        prev_total_jiffies = cur_total_jiffies;
    }

    return 0;
}
