
#include <stdio.h>
#include <string.h>
#include <math.h>

#include "feature-analyzer.h"

mcv_cvp_feature_tracker_config_t init_default_feature_tracker_config(int width, int height) {

    // configuration object for feature tracking
    mcv_cvp_feature_tracker_config_t feature_tracker_config;

    // which octave of the pyramid to perform optic flow on
    feature_tracker_config.opt_flow_octave = 1;
    feature_tracker_config.two_img_input = false;

    // create a binary mask for features (NULL implies all 1's)
    feature_tracker_config.feature_mask = NULL;

    // populate feature tracker settings
    feature_tracker_config.pyr_fpx_config = mcv_cvp_pyr_fpx_default_config();

    feature_tracker_config.opt_flow_config.width =
        width / pow(2, feature_tracker_config.opt_flow_octave);
    feature_tracker_config.opt_flow_config.height =
        height / pow(2, feature_tracker_config.opt_flow_octave);
    feature_tracker_config.opt_flow_config.en_stats = 0;
    feature_tracker_config.opt_flow_config.input_ubwc = 0;

    return feature_tracker_config;
}


void get_metrics_from_vft_data(std::map<int, vft_metrics_t>& vft_map, 
                               vft_feature_packet* feature_packet,
                               vft_feature* features, int n_rec_packets) {

    // count total amount of features across all cams
    int n_total_features = 0;
    for (int i = 0; i < n_max_cameras; i++) {
        if ((1U << i) & feature_packet->cams_with_data) {
            n_total_features += feature_packet->num_feats[i];
        }
    }

    // loop over all the features and updates vft metrics
    for (int i = 0; i < n_total_features; i++) {
        int cam_id = features[i].cam_id;

        // if the age is over the age threshold, include the feature in metrics
        if (features[i].age > 10) {
            vft_map[cam_id].feature_age_sum += features[i].age;
            vft_map[cam_id].n_features++;
        }
    }

}

// writes the accumulated metrics to JSON
int _write_metrics_to_json(std::string output_path, std::map<int, vft_metrics_t> vft_map) {
    int ret = 0;

    // if config file does not exist, make one and initialize it with a header
    ret = json_make_empty_file_with_header_if_missing(output_path.c_str(), "");
    if (ret < 0) return -1;

    // get parent JSON for reading
    cJSON *parent = json_read_file(output_path.c_str());

    if (parent == NULL) return -1;

    cJSON *feature_age_sum  = cJSON_CreateObject();
    cJSON *feature_age_mean = cJSON_CreateObject();


    for (const auto& pair : vft_map) {
        int cam_id = pair.first;
        vft_metrics_t metrics = pair.second;

        // compute the feature age mean, write to JSON
        double sum = (double)metrics.feature_age_sum;
        double mean = sum / metrics.n_features;

        // add the total feature age to the json
        std::string str = "cam_" + std::to_string(cam_id);
        cJSON_AddNumberToObject(feature_age_sum, str.c_str(), sum);

        // add the mean feature age to the json
        cJSON_AddNumberToObject(feature_age_mean, str.c_str(), mean);
    }

    // record time and date of test completion
    std::time_t now = std::time(nullptr);
    char date_time[80];
    std::strftime(date_time, sizeof(date_time), 
                  "%Y-%m-%dT%H:%M:%S", std::localtime(&now));
    cJSON_AddStringToObject(parent, "date_time", date_time);

    // add all the metrics objects to the json
    cJSON_AddItemToObject(parent, "feature_age_sum", feature_age_sum);
    cJSON_AddItemToObject(parent, "feature_age_mean", feature_age_mean);

    // check if we got any errors in that process
    if (json_get_parse_error_flag()) {
        fprintf(stderr, "failed to parse data in %s\n", output_path);
        cJSON_Delete(parent);
        return -1;
    }

    // write to json file
    json_write_to_file_with_header(output_path.c_str(), parent, "");
    cJSON_Delete(parent);


    fprintf(stderr, "Test Output: %s\n", output_path.c_str());
    return ret;
}