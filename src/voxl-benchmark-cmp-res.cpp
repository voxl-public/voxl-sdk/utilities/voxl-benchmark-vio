// Include necessary headers
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cstring>
#include <map>
#include <set>
#include <vector>
#include <cmath>
#include <limits>
#include <cJSON.h>

// For convenience
using namespace std;

// Function to read a file into a string
string read_file(const string& filename) {
    ifstream file(filename);
    if (!file.is_open()) {
        cerr << "Failed to open " << filename << endl;
        exit(EXIT_FAILURE);
    }
    stringstream buffer;
    buffer << file.rdbuf();
    return buffer.str();
}

// Function to parse cJSON object into a map<string, double> and record key order
void parse_cjson_to_map(cJSON* json_obj, map<string, double>& result_map, vector<string>& key_order) {
    cJSON* item = nullptr;
    cJSON_ArrayForEach(item, json_obj) {
        if (item->string && cJSON_IsNumber(item)) {
            result_map[item->string] = item->valuedouble;
            key_order.push_back(item->string);
        }
    }
}

// Function to compute differences between two maps
void compute_differences(const map<string, double>& map1, const map<string, double>& map2, map<string, string>& diff_map) {
    // Iterate over keys in map1
    for (const auto& entry : map1) {
        const string& key = entry.first;
        double val1 = entry.second;

        auto it = map2.find(key);
        if (it != map2.end()) {
            // Key exists in both maps
            double val2 = it->second;
            double diff_value = val1 - val2;
            diff_map[key] = to_string(diff_value);
        } else {
            // Key missing in map2
            diff_map[key] = "Key missing in JSON 2";
        }
    }

    // Find keys in map2 that are missing in map1
    for (const auto& entry : map2) {
        const string& key = entry.first;
        if (map1.find(key) == map1.end()) {
            // Key missing in map1
            diff_map[key] = "Key missing in JSON 1";
        }
    }
}

int main(int argc, char* argv[]) {
    // Check if the correct number of arguments is provided
    if (argc != 3) {
        cerr << "Usage: " << argv[0] << " <json_file1> <json_file2>" << endl;
        return EXIT_FAILURE;
    }

    // Get JSON file names from command-line arguments
    string file1_name = argv[1];
    string file2_name = argv[2];

    // Read JSON files into strings
    string json_str1 = read_file(file1_name);
    string json_str2 = read_file(file2_name);

    // Parse JSON strings
    cJSON* json1 = cJSON_Parse(json_str1.c_str());
    cJSON* json2 = cJSON_Parse(json_str2.c_str());

    if (!json1 || !json2) {
        cerr << "Error parsing JSON files." << endl;
        cJSON_Delete(json1);
        cJSON_Delete(json2);
        return EXIT_FAILURE;
    }

    // Map to hold the differences
    map<string, map<string, string>> diff;

    // Map to hold the key order for each log
    map<string, vector<string>> key_orders;

    // Assuming the JSON files are arrays
    cJSON* item1 = nullptr;
    cJSON_ArrayForEach(item1, json1) {
        cJSON* item2 = nullptr;
        cJSON_ArrayForEach(item2, json2) {
            // Get the key (assuming each object has one key)
            string key1 = item1->child && item1->child->string ? item1->child->string : "";
            string key2 = item2->child && item2->child->string ? item2->child->string : "";

            if (key1 == key2 && !key1.empty()) {
                // Parse the cJSON objects into maps and record key order
                map<string, double> map1, map2;
                vector<string> key_order;
                cJSON* value1 = cJSON_GetObjectItem(item1, key1.c_str());
                cJSON* value2 = cJSON_GetObjectItem(item2, key2.c_str());
                parse_cjson_to_map(value1, map1, key_order);
                parse_cjson_to_map(value2, map2, key_order); // May include keys from value2

                // Remove duplicate keys in key_order
                vector<string> unique_key_order;
                set<string> seen_keys;
                for (const auto& k : key_order) {
                    if (seen_keys.find(k) == seen_keys.end()) {
                        unique_key_order.push_back(k);
                        seen_keys.insert(k);
                    }
                }

                // Compute differences
                map<string, string> diff_map;
                compute_differences(map1, map2, diff_map);

                // Store differences and key order under the log key
                diff[key1] = diff_map;
                key_orders[key1] = unique_key_order;
            }
        }
    }

    // Convert the diff map to a cJSON object
    cJSON* diff_json = cJSON_CreateObject();

    string comparison_message = "Comparing change from '" + file1_name + "' to '" + file2_name + "'";
    cJSON_AddStringToObject(diff_json, "comparison", comparison_message.c_str());

    for (const auto& log_entry : diff) {
        const string& log_key = log_entry.first;
        const map<string, string>& metrics = log_entry.second;
        const vector<string>& key_order = key_orders[log_key];

        cJSON* metrics_obj = cJSON_CreateObject();
        for (const auto& metric_key : key_order) {
            auto it = metrics.find(metric_key);
            if (it != metrics.end()) {
                const string& diff_value = it->second;

                // Check if the difference is numeric
                char* end;
                double val = strtod(diff_value.c_str(), &end);
                if (end != diff_value.c_str() && *end == '\0') {
                    // It's a number
                    cJSON_AddNumberToObject(metrics_obj, metric_key.c_str(), val);
                } else {
                    // It's a string (e.g., "Key missing in JSON 2")
                    cJSON_AddStringToObject(metrics_obj, metric_key.c_str(), diff_value.c_str());
                }
            }
        }

        cJSON_AddItemToObject(diff_json, log_key.c_str(), metrics_obj);
    }

    // Write the differences to a new JSON file
    char* diff_str = cJSON_Print(diff_json);
    ofstream diff_file("differences.json");
    diff_file << diff_str << endl;

    cout << "Differences have been written to differences.json" << endl;

    // Cleanup
    cJSON_Delete(json1);
    cJSON_Delete(json2);
    cJSON_Delete(diff_json);
    cJSON_free(diff_str);

    return 0;
}
