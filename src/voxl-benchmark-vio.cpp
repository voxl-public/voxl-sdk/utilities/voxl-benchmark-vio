#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <string.h>
#include <iostream>
#include <unistd.h>
#include <sys/wait.h>
#include <cstdlib>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>

#include <modalcv.h>

#include "benchmark-utility.h"
#include "cpu-usage-utils.h"

typedef enum command_num_t {
    VOXL_FEATURE_ANALYZER = 0,
    VOXL_FEATURE_TRACKER = 1,
    VOXL_REPLAY = 2,
    N_COMMANDS_TO_RUN
} command_num_t;

const std::string BENCHMARK_LOGS_FOLDER_PATH = "/data/benchmark-logs/";
const std::string BENCHMARK_RESULT_FOLDER_PATH = "/data/benchmark-results/";

const int max_path_size = 80;
bool en_feature_metrics = false;
bool en_download_if_missing = false;
std::vector<int> json_replay_index;
std::string input_path;
std::string output_path;

static void _print_usage(void) {
    printf(
"Benchmark for feature tracker\n\
\n\
-h, --help                  print this help message\n\
-p, --path {path}           complete path to json, e.g. /data/myfile.json\n\
-i, --index {index}         runs benchmark on only this index in the json file,\n\
                            by default, run replay on all logs in the json\n\
-o, --output {name}         name of output folder created in /data/benchmark-results/\n\
-f                          include vft metrics in the output json\n\
-d                          automatically downloads logs if not on local\n\
\n");
return;
}

static int _parse_opts(int argc, char *argv[]) {
    static struct option long_options[] = {
        {"help", no_argument, 0, 'h'},
        {"path", required_argument, 0, 'p'},
        {"index", required_argument, 0, 'i'},
        {"output", required_argument, 0, 'o'},
        {"features", no_argument, 0, 'f'},
        {"auto_download", no_argument, 0, 'd'},
        {0, 0, 0, 0}
    };

    int path_flag = 0;  // flag to check if path option is provided
    int output_flag = 0;
    int index_flag = 0;

    while (1) {
        int option_index = 0;
        int c = getopt_long(argc, argv, "hp:o:fi:d", long_options, &option_index);

        if (c == -1) break;  // Detect the end of the options.

        switch (c) {
            case 'p':
                path_flag = 1;
                printf("Input JSON: %s\n", optarg);
                input_path = optarg;
                break;
            case 'o':
                output_flag = 1;
                printf("Output JSON: %s\n", optarg);
                output_path = optarg;
                break;
            case 'i':
                index_flag = 1;
                json_replay_index.push_back(std::atoi(optarg));
                break;
            case 'd':
                en_download_if_missing = true;
                break;
            case 'f':
                en_feature_metrics = true;
                break;
            case 'h':
                _print_usage();
                return 1;
            case '?':
                // If option is missing argument or unknown option
                _print_usage();
                return 1;
            default:
                _print_usage();
                return 1;
        }
    }

    // check if path and output option are provided
    if (!path_flag || !output_flag) {
        printf("Error: Missing or incorrect arguments.\n");
        _print_usage();
        return 1;
    }
    return 0;
}


void set_conf_file_settings(bool en_ext_tracker, bool en_gpu, std::string cfg_path);
void set_tracking_pipe_to_base_name(std::string cfg_path);


int main(__attribute__((unused)) int argc,
         __attribute__((unused)) char *argv[]) {

    if (_parse_opts(argc, argv) != 0) {
        return -1;
    }

    // parse the input json for log info
    std::vector<log_test_struct_t> tests = parse_log_test_json_file(std::string(input_path));

    // make considerations for only including certain indices
    if (!json_replay_index.empty()) {
        std::vector<log_test_struct_t> temp;
        for (const auto& idx : json_replay_index) {
            temp.push_back(tests[idx]);
        }
        tests = temp;
    }

    // check if logs are installed on local, if not, download them
    if (en_download_if_missing) {
        for (const auto& log : tests) {
            std::string log_id = log.id;
            std::string log_path = concatenate_paths(BENCHMARK_LOGS_FOLDER_PATH, log_id);

            if (!directory_exists(log_path)) {
                printf("Log not found: %s\n", log_path.c_str());
                // std::string command = "wget -P " + BENCHMARK_LOGS_FOLDER_PATH + " " + log.url;
                // int result = std::system(command.c_str());

                // if (result == 0) {
                //     printf("Successfully Downloaded: %s\n", log.url.c_str());
                // } else {
                //     printf("Failed to Download: %s\n", log.url.c_str());
                //     return 0;
                // }

                // if (!create_directories(log_path)) {
                //     return -1; // Failed to create directory
                // }

                // std::string tar_path = log_path + ".tar.gz";
                // command = "tar -xzf " + tar_path + " -C " + log_path;

                // result = std::system(command.c_str());

                // // Check if the command was successful
                // if (result == 0) {
                //     std::cout << "Extracted " << tar_path << " to " << log_path << std::endl;
                // } else {
                //     std::cerr << "Failed to extract " << tar_path << std::endl;
                //     return 0;
                // }

                // command = "rm " + tar_path;
                // result = std::system(command.c_str());

                // printf("Downloaded and unpacked: %s\n", log.url.c_str());
            } 
            else {
                printf("Log found: %s\n", log_path.c_str());
            }
        }
    }

    // makes a temp folder to store individual log results before they are combined
    std::string temp_output_dir = BENCHMARK_RESULT_FOLDER_PATH + "benchmark_temp/";
    if (!directory_exists(temp_output_dir)) {
        if (!create_directories(temp_output_dir)) {
            std::cerr << "Failed to create temp output directory: " << temp_output_dir << std::endl;
            return -1;
        }
    }

    // makes a temp folder for config files which are restored later on
    std::string temp_etc_config = BENCHMARK_LOGS_FOLDER_PATH + "etc_config_temp/";
    std::string temp_data_config = BENCHMARK_LOGS_FOLDER_PATH + "data_config_temp/";

    if (!directory_exists(temp_etc_config)) {
        if (!create_directories(temp_etc_config)) {
            std::cerr << "Failed to create temp output directory: " << temp_etc_config << std::endl;
            return -1;
        }
    }
    if (!directory_exists(temp_data_config)) {
        if (!create_directories(temp_data_config)) {
            std::cerr << "Failed to create temp output directory: " << temp_data_config << std::endl;
            return -1;
        }
    }

    // copy local files to temp directories
    copy_folder_with_extension(std::string("/data/modalai/"), temp_data_config, std::string("yml"));
    copy_folder_with_extension(std::string("/etc/modalai/"), temp_etc_config, std::string("conf"));


    for (const auto& test : tests) {
        std::string log_id = test.id;
        std::string log_path = BENCHMARK_LOGS_FOLDER_PATH + log_id + "/";

        std::string tracker_mode_str = test.en_ext_tracker ? "_ext_tracker" : (test.en_gpu ? "_int_tracker_gpu" : "_int_tracker");
        std::string vft_output_json = concatenate_paths(temp_output_dir, log_id + tracker_mode_str + "_vft.json");
        std::string vio_output_json = concatenate_paths(temp_output_dir, log_id + tracker_mode_str + "_vio.json"); 

        std::string etc_folder  = log_path + std::string("etc/modalai/");
        std::string data_folder = log_path + std::string("data/modalai/");
        
        // copy .conf from log folder to local /etc/modalai
        copy_folder_with_extension(etc_folder, std::string("/etc/modalai/"), std::string("conf"));
        copy_folder_with_extension(data_folder, std::string("/data/modalai/"), std::string("yml"));

        set_conf_file_settings(test.en_ext_tracker, test.en_gpu, std::string("/etc/modalai/voxl-open-vins-server.conf"));
        printf("EN_GPU: %d\n", test.en_gpu);
        if (!test.en_ext_tracker && !test.en_gpu) {
            set_tracking_pipe_to_base_name("/etc/modalai/vio_cams.conf");
        }

        printf("Starting test: %s\n", log_path.c_str());

        // store the PIDs of the child processes
        pid_t pid_feature_analyzer;
        pid_t pid_vio_analyzer;
        pid_t pid_ovins;
        pid_t pid_vft; 
        pid_t pid_voxl_replay;

        // spawn all child processes

        if (test.en_ext_tracker) {

            // voxl-feature-analyzer
            if (en_feature_metrics) {
                pid_feature_analyzer = fork();
                if (pid_feature_analyzer == 0) {
                    execlp("voxl-feature-analyzer", "voxl-feature-analyzer", "-o", vft_output_json.c_str(), nullptr);
                    std::cerr << "Error executing command: voxl-feature-analyzer" << std::endl;
                    exit(1);
                } else if (pid_feature_analyzer < 0) {
                    std::cerr << "Fork failed for voxl-feature-analyzer" << std::endl;
                    exit(1);
                }
                usleep(500000);
            }

            // voxl-feature-tracker
            pid_vft = fork();
            if (pid_vft == 0) {
                execlp("voxl-feature-tracker", "voxl-feature-tracker", nullptr);
                std::cerr << "Error executing command: voxl-feature-tracker" << std::endl;
                exit(1);
            } else if (pid_vft < 0) {
                std::cerr << "Fork failed for voxl-feature-tracker" << std::endl;
                exit(1);
            }
            usleep(500000);

        }

        // voxl-vio-analyzer
        pid_vio_analyzer = fork();
        if (pid_vio_analyzer == 0) {
            execlp("voxl-vio-analyzer", "voxl-vio-analyzer", "-o", vio_output_json.c_str(), nullptr);
            std::cerr << "Error executing command: voxl-vio-analyzer" << std::endl;
            exit(1);
        } else if (pid_vio_analyzer < 0) {
            std::cerr << "Fork failed for voxl-vio-analyzer" << std::endl;
            exit(1);
        }
        usleep(500000);


        // voxl-open-vins-server
        pid_ovins = fork();
        if (pid_ovins == 0) {
            execlp("voxl-open-vins-server", "voxl-open-vins-server", "-o", nullptr);
            std::cerr << "Error executing command: voxl-open-vins-server" << std::endl;
            exit(1);
        } else if (pid_ovins < 0) {
            std::cerr << "Fork failed for voxl-open-vins-server" << std::endl;
            exit(1);
        }
        usleep(1000000);

        // create thread to track cpu usage of ovins
        cpu_usage_data ovins_cpu_data;
        std::thread cpu_thread(track_cpu_usage, pid_ovins, &ovins_cpu_data);


        std::vector<std::string> replay_command = {"voxl-replay", "-p", log_path, "-y"};
        for (const auto& pipe : test.pipe_includes) {
            replay_command.push_back("-i");
            replay_command.push_back(pipe);
        }

        // Build the argument list for execvp
        std::vector<char*> args;
        for (const auto& param : replay_command) {
            // printf("param: %s\n", param.c_str());
            args.push_back(const_cast<char*>(param.c_str()));
        }

        args.push_back(nullptr);

        // voxl-replay
        pid_voxl_replay = fork();
        if (pid_voxl_replay == 0) {
            execvp("voxl-replay", args.data());
            std::cerr << "Error executing command: voxl-replay" << std::endl;
            exit(1);
        } else if (pid_voxl_replay < 0) {
            std::cerr << "Fork failed for voxl-replay" << std::endl;
            exit(1);
        }


        // parent process: Wait for voxl-replay to finish   
        int status;
        waitpid(pid_voxl_replay, &status, 0);
        if (WIFEXITED(status)) {
            std::cout << "voxl-replay process (PID: " << pid_voxl_replay << ") exited successfully." << std::endl;
        } else {
            std::cerr << "voxl-replay process (PID: " << pid_voxl_replay << ") failed." << std::endl;
        }

        // signal processes to terminate
        kill(pid_vio_analyzer, SIGTERM);
        if (en_feature_metrics) {
            kill(pid_feature_analyzer, SIGTERM);
        }
        kill(pid_ovins, SIGTERM);
        kill(pid_vft, SIGTERM);

        ovins_cpu_data.keep_running = false;
        cpu_thread.join();

        double median_usage = compute_median(ovins_cpu_data.usage_samples);
        std::cout << "Median CPU usage for PID " << pid_ovins << ": " 
              << median_usage << "%\n";



        if (ovins_cpu_data.samples > 0) {
            double avgUsage = 100.0 * ovins_cpu_data.usage_sum / ovins_cpu_data.samples;
            std::cout << "Average CPU usage for voxl-open-vins-server [PID " 
                    << pid_ovins << "]: " << avgUsage << "%\n";
        } else {
            std::cout << "No CPU samples were taken for PID " << pid_ovins << "\n";
        }


        // wait for processes to terminate
        waitpid(pid_vio_analyzer, nullptr, 0);
        
        if (en_feature_metrics) {
            waitpid(pid_feature_analyzer, nullptr, 0);
        }

        waitpid(pid_ovins, nullptr, 0);

        if (test.en_ext_tracker) {
            waitpid(pid_vft, nullptr, 0);
        }

        usleep(1000000);
    }

    std::vector<std::string> json_files = get_files_in_directory(temp_output_dir);
    std::sort(json_files.begin(), json_files.end());

    // combine the resulting output jsons into a summary
    cJSON* combined_json = cJSON_CreateObject();
    if (!combined_json) {
        std::cerr << "Failed to create combined JSON object" << std::endl;
        return 0;
    }

    for (const auto& json_path : json_files) {
        cJSON* current_json = read_json_file(json_path);
        if (current_json) {
            // Add the current JSON to the combined JSON
            std::string name = split_string(json_path, '/').back();
            cJSON_AddItemToObject(combined_json, name.c_str(), current_json);
        }
    }

    // write out the combined JSON
    output_path = BENCHMARK_RESULT_FOLDER_PATH + output_path;
    write_summary_json(output_path, combined_json);

    // Clean up
    cJSON_Delete(combined_json);

    // restore original config files
    copy_folder_with_extension(temp_data_config, std::string("/data/modalai/"), std::string("yml"));
    copy_folder_with_extension(temp_etc_config, std::string("/etc/modalai/"), std::string("conf"));


    std::vector<std::string> folders_to_remove = {
        temp_output_dir,
        temp_data_config,
        temp_etc_config
    };

    for (const auto& dir : folders_to_remove) {
        // Remove the folder and all its contents
        std::error_code ec; // to capture any errors
        fs::remove_all(dir, ec);

        if (ec) {
            std::cerr << "Error deleting folder: " << ec.message() << std::endl;
        }
    }

    return 0;
}


void write_file(const std::string& filename, const std::string& comment, const std::string& json_data) {
    std::ofstream file(filename, std::ios::binary);
    if (!file) {
        std::cerr << "Error opening file " << filename << " for writing.\n";
        return;
    }
    file << comment;
    file << json_data;
}

void set_tracking_pipe_to_base_name(std::string cfg_path) {

    std::string json_content = read_file(cfg_path);
    if (json_content.empty()) {
        printf("file was empty\n");
        return;
    }

    cJSON *json = cJSON_Parse(json_content.c_str());
    if (!json) {
        printf("Error parsing JSON content.\n");
        return;
    }

    cJSON *cams = cJSON_GetObjectItem(json, "cams");
    if (cams == NULL) {
        printf("Error: No cams array found in the JSON\n");
        cJSON_Delete(json);
        return;
    }

    // Iterate through each camera in the array
    int cams_count = cJSON_GetArraySize(cams);
    for (int i = 0; i < cams_count; i++) {
        cJSON *cam = cJSON_GetArrayItem(cams, i);
        
        // Get the 'name' field
        cJSON *name = cJSON_GetObjectItem(cam, "name");
        if (name && cJSON_IsString(name)) {
            // Create a new string value for 'pipe_for_tracking' based on 'name'
            char *new_pipe_value = name->valuestring;
            
            // Find the 'pipe_for_tracking' field and update it
            cJSON *pipe_for_tracking = cJSON_GetObjectItem(cam, "pipe_for_tracking");
            if (pipe_for_tracking && cJSON_IsString(pipe_for_tracking)) {
                cJSON_SetValuestring(pipe_for_tracking, new_pipe_value);
            }
        }
    }

    char *modified_json_char = cJSON_Print(json);
    cJSON_Delete(json);
    if (!modified_json_char) {
        std::cerr << "Failed to print JSON.\n";
        return;
    }
    std::string modified_json = modified_json_char;
    cJSON_free(modified_json_char);

    // Step 6: Write the modified content back to the file
    write_file(cfg_path, "", modified_json);

    return;
}

void set_conf_file_settings(bool en_ext_tracker, bool en_gpu, std::string cfg_path) {

    cJSON_bool target_value;
    if (en_ext_tracker) {
        target_value = cJSON_True;
    } else {
        target_value = cJSON_False;
    }

    cJSON_bool en_gpu_val;
    if (en_gpu) {
        en_gpu_val = cJSON_True;
    } else {
        en_gpu_val = cJSON_False;
    }

    // Read the file content
    std::string file_content = read_file(cfg_path);
    if (file_content.empty()) {
        printf("file was empty\n");
        return;
    }

    size_t json_start_pos = file_content.find('{');
    if (json_start_pos == std::string::npos) {
        std::cerr << "No JSON content found in the file.\n";
        return;
    }

    // Extract the comment
    std::string comment = file_content.substr(0, json_start_pos);

    // Extract the JSON content
    std::string json_content = file_content.substr(json_start_pos);

    // Parse the JSON content
    cJSON *json = cJSON_Parse(json_content.c_str());
    if (!json) {
        std::cerr << "Error parsing JSON content.\n";
        return;
    }

    // Set the "en_ext_feature_tracker" value
    cJSON *tracker_item = cJSON_GetObjectItemCaseSensitive(json, "en_ext_feature_tracker");
    if (tracker_item && cJSON_IsBool(tracker_item)) {
        // Set the value to the specified target value
        tracker_item->type = target_value;
    } else {
        // If the item doesn't exist or isn't a boolean, add or replace it
        if (tracker_item) {
            cJSON_DeleteItemFromObjectCaseSensitive(json, "en_ext_feature_tracker");
        }
        cJSON_AddBoolToObject(json, "en_ext_feature_tracker", target_value);
    }

    // Set the "en_gpu_for_tracking" value
    cJSON *gpu_item = cJSON_GetObjectItemCaseSensitive(json, "en_gpu_for_tracking");
    if (gpu_item && cJSON_IsBool(gpu_item)) {
        // Set the value to the specified target value
        gpu_item->type = en_gpu_val;
    } else {
        // If the item doesn't exist or isn't a boolean, add or replace it
        if (gpu_item) {
            cJSON_DeleteItemFromObjectCaseSensitive(json, "en_gpu_for_tracking");
        }
        cJSON_AddBoolToObject(json, "en_gpu_for_tracking", en_gpu_val);
    }

    // Convert JSON object back to string
    char *modified_json_char = cJSON_Print(json);
    cJSON_Delete(json);
    if (!modified_json_char) {
        std::cerr << "Failed to print JSON.\n";
        return;
    }
    std::string modified_json = modified_json_char;
    cJSON_free(modified_json_char);

    // Write the modified content back to the file
    write_file(cfg_path, comment, modified_json);

    std::cout << "Successfully set 'en_ext_feature_tracker' to " << en_ext_tracker << " in the file.\n";

    return;
}