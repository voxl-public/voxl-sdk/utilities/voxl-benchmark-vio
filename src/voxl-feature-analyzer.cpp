/*******************************************************************************
 * Copyright 2024 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

/*
 * @file        voxl-feature-analyzer.cpp
 *
 * @brief       Listens to the output of feature tracker and compiles
 *              metrics for writing to JSON
 *
 * @authors      Thomas Patton (thomas.patton@modalai.com)
 *               Kyle Tyni (kyle.tyni@modalai.com)
 * @date        2024
 */

#include <cJSON.h>
#include <getopt.h>
#include <modal_json.h>
#include <modal_pipe.h>
#include <modal_pipe_client.h>
#include <modal_start_stop.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>  // for usleep()
#include <ctime>
#include <map>
#include <vft_interface.h>
#include "feature-analyzer.h"


const int   max_path_size   = 64;
const int   TOP_FEATURES_N  = 20;
const char *CLIENT_NAME     = "voxl-feature-analyzer";
const char *IN_PIPE_NAME    = "tracking_feats";

// where to save the output JSON object
char output_path[max_path_size];

// maps cam_id (based on voxl-feature_tracker.conf) to relevant vft metrics
std::map<int, vft_metrics_t> vft_map;

// running count of how many vft packets have been received
int n_rec_packets = 0;

// structs for feature analysis
vft_feature_packet *feature_packet;
vft_feature *features;

// strings for JSON file writing
const char *data_file_header = "";


// print usage of the module
static void _print_usage(void) {
    printf(
        "\n\
Tool to print data being delivered to the feature tracker pipe\n\
\n\
Note: This module isn't meant to be run on its own, it's invoked\n\
by voxl-benchmark-vio\n\
\n\
-o, --output            output file to write to\n\
-h, --help              print this help message\n\
\n\
\n");
    return;
}

// parse user-provided options
static int _parse_opts(int argc, char *argv[]) {
    static struct option long_options[] = {
        {"output", required_argument, 0, 'o'},
        {"help", no_argument, 0, 'h'},
        {0, 0, 0, 0}};

    int output_flag = 0;  // flag to check if path option is provided

    while (1) {
        int option_index = 0;
        int c = getopt_long(argc, argv, "ho:", long_options, &option_index);

        if (c == -1) break;  // Detect the end of the options.

        switch (c) {
            case 0:
                // for long args without short equivalent that just set a flag
                // nothing left to do so just break.
                if (long_options[option_index].flag != 0) break;
                break;
            case 'o':
                output_flag = 1;
                printf("Save path: %s\n", optarg);
                strncpy(output_path, optarg, max_path_size);
                break;
            case 'h':
                _print_usage();
                exit(0);
            case '?':
                // If option is missing argument or unknown option
                _print_usage();
                exit(0);
            default:
                _print_usage();
                exit(0);
        }
    }

    // check if path option is provided
    if (!output_flag) {
        printf("Error: Path option is mandatory.\n");
        _print_usage();
        return -1;
    }
    return 0;
}


// called whenever we connect or reconnect to the server
static void _connect_cb(__attribute__((unused)) int ch,
                        __attribute__((unused)) void *context) {
    // get some space for the features
    create_vft_memory(&feature_packet, &features);
    return;
}


// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch,
                           __attribute__((unused)) void *context) {
    // destroy feature memory
    destroy_vft_memory(&feature_packet, &features);
    return;
}


// called when vft data is ready
static void _helper_cb(__attribute__((unused)) int ch, char *data, int bytes,
                       __attribute__((unused)) void *context) {

    // use validation function to extract out packet/feature data from pipe
    if (validate_vft_data(ch, data, bytes, &feature_packet, &features)) {
        printf("ERROR parsing vft data from pipe...\n");
        return;
    }

    // increment the number of received vft packets
    n_rec_packets++;

    get_metrics_from_vft_data(vft_map, feature_packet, features, n_rec_packets);

    return;
}


int main(__attribute__((unused)) int argc,
         __attribute__((unused)) char *argv[]) {
    int ret = 0;

    // parse user-provided options
    if (_parse_opts(argc, argv)) return -1;

    enable_signal_handler();

    // set up all our MPA callbacks
    pipe_client_set_simple_helper_cb(0, _helper_cb, NULL);
    pipe_client_set_connect_cb(0, _connect_cb, NULL);
    pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);
    // create_vft_memory(&feature_packet, &features);

    ret = pipe_client_open(0, IN_PIPE_NAME, CLIENT_NAME,
                           EN_PIPE_CLIENT_SIMPLE_HELPER,
                           sizeof(vft_feature_packet));
    main_running = 1;

    if (ret) {
        fprintf(stderr, "ERROR: failed to open pipe %s\n", IN_PIPE_NAME);
        pipe_print_error(ret);
        fprintf(stderr, "Is voxl-feature-tracker running?\n");
        return ret;
    }

    // check for errors trying to connect to the server pipe
    if (ret < 0) {
        pipe_print_error(ret);
        return -1;
    }

    // keep going until the  signal handler sets the running flag to 0
    while (main_running) usleep(100000);

    // all done, signal pipe read threads to stop
    pipe_client_close_all();

    // done, so write out metrics to JSON
    _write_metrics_to_json(std::string(output_path), vft_map);

    return ret;
}
