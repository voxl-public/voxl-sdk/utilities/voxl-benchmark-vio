/*******************************************************************************
 * Copyright 2024 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

/*
 * @file        voxl-vio-analyzer.cpp
 *
 * @brief       Listens to the output of open vins and compiles
 *              metrics for writing to JSON
 *
 * @author      Thomas Patton (thomas.patton@modalai.com)
 * @date        2024
 */

#include <cJSON.h>
#include <getopt.h>
#include <modal_json.h>
#include <modal_pipe_client.h>
#include <modal_pipe_interfaces.h>
#include <modal_start_stop.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>  // for usleep()
#include <cmath>
#include <fstream>
#include <iomanip>
#include <sstream>
#include <ctime>

const int max_path_size = 120;
const char* CLIENT_NAME = "voxl-vio-analyzer";
const char* IN_PIPE_NAME = "/run/mpa/ov";
static std::ofstream csv_file;

static void _open_csv_file(const char* filename) {
    csv_file.open(filename, std::ios::out | std::ios::trunc);
    if (!csv_file.is_open()) {
        fprintf(stderr, "Error: Unable to open file %s for writing.\n", filename);
        return;
    }
    // Write header
    csv_file << "frame_number,timestamp_ns,quality\n";
}

static void _close_csv_file() {
    if (csv_file.is_open()) {
        csv_file.close();
    }
}

std::string get_current_timestamp() {
    std::time_t now = std::time(nullptr);
    std::tm* local_tm = std::localtime(&now);

    std::ostringstream oss;
    oss << std::put_time(local_tm, "%Y%m%d_%H%M%S");  // YYYYMMDD_HHMMSS
    return oss.str();
}



std::string get_csv_path(const char* json_path) {
    std::string path(json_path);

    // get the last '/' to locate the directory
    size_t last_slash = path.find_last_of('/');
    if (last_slash == std::string::npos) {
        return "/data/benchmark-results/default_quality.csv";  // fallback
    }

    // extract the directory path up to 'benchmark_temp'
    std::string dir_path = path.substr(0, last_slash);  // remove filename

    // find the last occurrence of '/' to remove 'benchmark_temp/'
    size_t temp_pos = dir_path.find_last_of('/');
    if (temp_pos != std::string::npos) {
        dir_path = dir_path.substr(0, temp_pos);  // remove 'benchmark_temp'
    }

    // extract the base filename without "_ext_tracker_vio.json"
    size_t start = path.find_last_of('/');
    std::string filename = path.substr(start + 1);
    size_t ext_pos = filename.find("_ext_tracker_vio.json");
    if (ext_pos != std::string::npos) {
        filename = filename.substr(0, ext_pos);  // Remove "_ext_tracker_vio.json"
    }
    
    //get timestamp
    std::string timestamp = get_current_timestamp();

    return dir_path + "/" + filename + "_quality_" + timestamp + ".csv";
}


// metrics to publish
struct vio_metrics_t {
    // where to save the output JSON object
    char output_path[max_path_size];

    // total count of frames seen
    int32_t n_frames;

    // total frames where quality was ok
    int32_t n_ok_q_frames;

    int32_t n_bad_q_frames;

    // total frames where quality was "low"
    int32_t n_low_q_frames;

    int32_t first_frame;

    // total summed quality over all frames
    int32_t quality_sum;

    // number of callbacks without
    int32_t n_frames_not_tracked;

    float xyz[3];
    float max_xyz[3];

    vio_metrics_t() : n_frames(0), quality_sum(0), n_frames_not_tracked(0), first_frame(0) {}
};

// structs for analysis
vio_data_t* vio_data;
vio_metrics_t vio_metrics;

// strings for JSON file writing
const char* data_file_header = "";

// print usage of the module
static void _print_usage(void) {
    printf(
        "\n\
Tool to print data being delivered to the feature tracker pipe\n\
\n\
Note: This module isn't meant to be run on its own, it's invoked\n\
by voxl-benchmark-vio\n\
\n\
-o, --output            output file to write to\n\
-h, --help              print this help message\n\
\n\
\n");
    return;
}

// called whenever we connect or reconnect to the server
static void _connect_cb(__attribute__((unused)) int ch,
                        __attribute__((unused)) void* context) {
    return;
}

// called whenever we disconnect from the server
static void _disconnect_cb(__attribute__((unused)) int ch,
                           __attribute__((unused)) void* context) {
    return;
}

static void _helper_cb(int ch, char* data, int bytes,
                       __attribute__((unused)) void* context) {
    int valid_n_packets;
    vio_data_t* vio_data = pipe_validate_vio_data_t(data, bytes, &valid_n_packets);
    if (NULL == vio_data) {
        pipe_client_flush(ch);
        return;
    }

    static bool first_ok_quality = false;

    for (int i = 0; i < valid_n_packets; i++) {
        
        vio_metrics.n_frames++;

        if (vio_data[i].state == VIO_STATE_OK && vio_data[i].quality > 0) {
            vio_metrics.quality_sum += vio_data[i].quality;
            vio_metrics.n_ok_q_frames++;

            if (!first_ok_quality) {
                first_ok_quality = true;
            }
        } 
        else {
            if (first_ok_quality) {
                vio_metrics.n_bad_q_frames++;

                if (vio_data[i].quality < 10) {
                    vio_metrics.n_low_q_frames++;
                }
            }
        }

        if (vio_metrics.first_frame == 0) {
            vio_metrics.first_frame = vio_data[i].timestamp_ns;
        }

        vio_metrics.xyz[0] = vio_data[i].T_imu_wrt_vio[0];
        vio_metrics.xyz[1] = vio_data[i].T_imu_wrt_vio[1];
        vio_metrics.xyz[2] = vio_data[i].T_imu_wrt_vio[2];

        if (abs(vio_metrics.xyz[0]) > abs(vio_metrics.max_xyz[0]))
            vio_metrics.max_xyz[0] = vio_metrics.xyz[0];

        if (abs(vio_metrics.xyz[1]) > abs(vio_metrics.max_xyz[1]))
            vio_metrics.max_xyz[1] = vio_metrics.xyz[1];
            
        if (abs(vio_metrics.xyz[2]) > abs(vio_metrics.max_xyz[2]))
            vio_metrics.max_xyz[2] = vio_metrics.xyz[2];

        if (csv_file.is_open()) {
            csv_file << vio_metrics.n_frames << ","
                     << vio_data[i].timestamp_ns << ","
                     << vio_data[i].quality << "\n";
        }
    }

    // validate that the data makes sense
    // int n_packets, i;
    // ext_vio_data_t* data_array =
    //     pipe_validate_ext_vio_data_t(data, bytes, &n_packets);
    // if (data_array == NULL) return;
    // for (i = 0; i < n_packets; i++) _print_data(data_array[i]);
    return;
}

static int _parse_opts(int argc, char* argv[]) {
    static struct option long_options[] = {
        {"output", required_argument, 0, 'o'},
        {"help", no_argument, 0, 'h'},
        {0, 0, 0, 0}};

    int output_flag = 0;  // flag to check if path option is provided

    while (1) {
        int option_index = 0;
        int c = getopt_long(argc, argv, "ho:", long_options, &option_index);

        if (c == -1) break;  // detect the end of the options

        switch (c) {
            case 0:
                // for long args without short equivalent that just set a flag
                // nothing left to do so just break.
                if (long_options[option_index].flag != 0) break;
                break;
            case 'o':
                output_flag = 1;
                printf("Save path: %s\n", optarg);
                strncpy(vio_metrics.output_path, optarg, max_path_size);
                break;
            case 'h':
                _print_usage();
                return -1;
            default:
                _print_usage();
                return -1;
        }
    }
    // check if path option is provided
    if (!output_flag) {
        printf("Error: Path option is mandatory.\n");
        _print_usage();
        return -1;
    }
    return 0;
}

// writes the accumulated metrics to JSON
static int _write_metrics_to_json() {
    int ret = 0;

    // if config file does not exist, make one and initialize it with a header
    ret = json_make_empty_file_with_header_if_missing(vio_metrics.output_path,
                                                      data_file_header);
    if (ret < 0) return -1;
    if (ret > 0)
        fprintf(stderr, "Creating new JSON file: %s\n",
                vio_metrics.output_path);

    // get parent JSON for reading
    cJSON* parent = json_read_file(vio_metrics.output_path);
    if (parent == NULL) return -1;

    // compute the quality mean, write to JSON
    double q_sum = (double)vio_metrics.quality_sum;
    cJSON_DeleteItemFromObject(parent, "vio_quality_sum");
    cJSON_AddNumberToObject(parent, "vio_quality_sum", q_sum);

    double q_mean = q_sum / vio_metrics.n_frames;
    cJSON_DeleteItemFromObject(parent, "vio_quality_mean");
    cJSON_AddNumberToObject(parent, "vio_quality_mean", q_mean);


    float x = vio_metrics.xyz[0];
    float y = vio_metrics.xyz[1];
    float z = vio_metrics.xyz[2];

    float max_x = vio_metrics.max_xyz[0];
    float max_y = vio_metrics.max_xyz[1];
    float max_z = vio_metrics.max_xyz[2];

    float final_pos_dist = std::sqrt(x * x + y * y + z * z);
    float max_dist = std::sqrt(max_x * max_x + max_y * max_y + max_z * max_z);

    float final_position_ratio = final_pos_dist / max_dist;

    cJSON_DeleteItemFromObject(parent, "pos_delta_ratio");
    cJSON_AddNumberToObject(parent, "pos_delta_ratio", final_position_ratio);

    cJSON_DeleteItemFromObject(parent, "final_x");
    cJSON_DeleteItemFromObject(parent, "final_y");
    cJSON_DeleteItemFromObject(parent, "final_z");
    cJSON_AddNumberToObject(parent, "final_x", x);
    cJSON_AddNumberToObject(parent, "final_y", y);
    cJSON_AddNumberToObject(parent, "final_z", z);

    cJSON_DeleteItemFromObject(parent, "max_x");
    cJSON_DeleteItemFromObject(parent, "max_y");
    cJSON_DeleteItemFromObject(parent, "max_z");
    cJSON_AddNumberToObject(parent, "max_x", max_x);
    cJSON_AddNumberToObject(parent, "max_y", max_y);
    cJSON_AddNumberToObject(parent, "max_z", max_z);

    cJSON_DeleteItemFromObject(parent, "n_frames");
    cJSON_AddNumberToObject(parent, "n_frames", vio_metrics.n_frames);

    cJSON_DeleteItemFromObject(parent, "n_ok_q_frames");
    cJSON_AddNumberToObject(parent, "n_ok_q_frames", vio_metrics.n_ok_q_frames);

    cJSON_DeleteItemFromObject(parent, "n_low_q_frames");
    cJSON_AddNumberToObject(parent, "n_low_q_frames", vio_metrics.n_low_q_frames);

    cJSON_DeleteItemFromObject(parent, "n_bad_q_frames");
    cJSON_AddNumberToObject(parent, "n_bad_q_frames", vio_metrics.n_bad_q_frames);

    // check if we got any errors in that process
    if (json_get_parse_error_flag()) {
        fprintf(stderr, "failed to parse data in %s\n",
                vio_metrics.output_path);
        cJSON_Delete(parent);
        return -1;
    }
    json_write_to_file_with_header(vio_metrics.output_path, parent,
                                   data_file_header);
    cJSON_Delete(parent);
    return ret;
}

int main(int argc, char* argv[]) {
    // check for options
    if (_parse_opts(argc, argv)) return -1;

    // set some basic signal handling for safe shutdown.
    // quitting without cleanup up the pipe can result in the pipe staying
    // open and overflowing, so always cleanup properly!!!
    enable_signal_handler();

    // set up all our MPA callbacks
    pipe_client_set_simple_helper_cb(0, _helper_cb, NULL);
    pipe_client_set_connect_cb(0, _connect_cb, NULL);
    pipe_client_set_disconnect_cb(0, _disconnect_cb, NULL);

    // request a new pipe from the server
    printf("waiting for server at %s\n", IN_PIPE_NAME);
    int ret = pipe_client_open(0, IN_PIPE_NAME, CLIENT_NAME,
                               EN_PIPE_CLIENT_SIMPLE_HELPER,
                               VIO_RECOMMENDED_READ_BUF_SIZE);
    main_running = 1;
    // check for MPA errors
    if (ret < 0) {
        pipe_print_error(ret);
        return -1;
    }

    std::string csv_file_path = get_csv_path(vio_metrics.output_path);
    _open_csv_file(csv_file_path.c_str());

    // keep going until signal handler sets the running flag to 0
    while (main_running) usleep(100000);

    // all done, signal pipe read threads to stop
    printf("\nclosing and exiting\n");
    pipe_client_close_all();

    // done, so write out metrics to JSON
    _write_metrics_to_json();
    _close_csv_file();
    return 0;
}
